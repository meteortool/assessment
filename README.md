# Meteor Assessment

Meteor is a tool that aims to support test code refactoring by using a mutation-based approach to assess test behavior during the refactoring process. Although it's currently in the development stage, this tool has the potential to significantly improve the efficiency and effectiveness of test code refactoring.

The project contains essential files for preliminary research and assessment of the proposed approach of tool usage.

# Paper
- [ ] [Link to paper] Missing

# Folder Structure

- [ ] [data](https://gitlab.com/meteortool/assessment/-/blob/main/data) - Contains all files of experiment. 
- [ ] [data\Comparativo.xslx](https://gitlab.com/meteortool/assessment/-/blob/main/data/Comparativo.xlsx) - Comparison of mutations in the three refactoring sections. 

# Authors and acknowledgment
Researchers: 

    Tiago Samuel Rodrigues Teixeira - 0009-0006-9960-4729  
    Fábio Fagundes Silveira - 0000-0002-2063-2959 
    Eduardo Martins Guerra  - 0000-0001-5555-3487